import React, { useState } from 'react';
import Modal from './components/Modal';
import ModalWrapper from './components/ModalWrapper';
import ModalHeader from './components/ModalHeader';
import ModalFooter from './components/ModalFooter';
import ModalClose from './components/ModalClose';
import ModalBody from './components/ModalBody';
import ModalImage from './components/ModalImage';
import ModalText from './components/ModalText';
import Button from './components/Button';
import './App.css';

const App = () => {
  const [showModalImage, setShowModalImage] = useState(false);
  const [showModalText, setShowModalText] = useState(false);

  const toggleModalImage = () => setShowModalImage(!showModalImage);
  const toggleModalText = () => setShowModalText(!showModalText);


  return (
    <>
    <div className="app">

      <Button onClick={toggleModalImage}>Open Modal One</Button>
      <Button onClick={toggleModalText}>Open Modal Two</Button>


      {showModalImage && (
        <ModalWrapper onClick={() => setShowModalImage(false)}>
          <Modal onClose={() => setShowModalImage(false)} title="Modal with Image">
            <ModalHeader>
              <ModalClose className="closeButton" onClick={() => setShowModalImage(false)} />
            </ModalHeader>
            <ModalBody>
              <div className="modal-image-container">
                <ModalImage src="path/to/image" alt="Modal Image" />
              </div>
              <h1>Product Delete!</h1>
              <br></br>
              <ModalText text="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted." />
            </ModalBody>
            <ModalFooter>
              <button onClick={() => setShowModalImage(false)} className="primaryButton">NO, CANCEL</button>
              <button onClick={() => setShowModalImage(false)} className="secondaryButton">YES, DELETE</button>
            </ModalFooter>
          </Modal>
        </ModalWrapper>
      )}


      {showModalText && (
        <ModalWrapper onClick={() => setShowModalText(false)}>
          <Modal onClose={() => setShowModalText(false)} title="Modal with Text">
            <ModalHeader>
              <ModalClose onClick={() => setShowModalText(false)} />
            </ModalHeader>
            <ModalBody>
            <h1>Add Product “NAME”</h1>
              <ModalText text="Description for you product" />
            </ModalBody>
            <ModalFooter>
              <button onClick={() => setShowModalText(false)} className="primaryButton">ADD TO FAVORITES</button>
            </ModalFooter>
          </Modal>
        </ModalWrapper>
      )}

    </div>

    </>
    
  );
};

export default App;

