import React from 'react';
import PropTypes from 'prop-types';
import './ModalHeader.scss';

const ModalHeader = ({ children }) => (
  <div className="modal-header">
    {children}
  </div>
);

ModalHeader.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ModalHeader;
