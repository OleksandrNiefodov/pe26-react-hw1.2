
import React from 'react';
import PropTypes from 'prop-types';
import './ModalText.scss';
const ModalText = ({ text }) => (
  <div className="modal-content">
    <div className="modal-body">
      <p>{text}</p>
    </div>
  </div>
);
ModalText.propTypes = {
  text: PropTypes.string.isRequired,
};
export default ModalText;